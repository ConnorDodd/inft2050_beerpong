﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using BeerPongGame.Model;
using System.Collections.Generic;
using BeerPongGame;
using Microsoft.WindowsAzure.MobileServices;

namespace BeerPong
{
    [Activity(Label = "BeerPong", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        public static MobileServiceClient MobileService =
            new MobileServiceClient(
            "https://beerpong.azurewebsites.net"
        );

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            RequestWindowFeature(WindowFeatures.NoTitle);
            SetContentView(Resource.Layout.Main);

            GameManager.LoadGames();

            Button postButton = FindViewById<Button>(Resource.Id.PostButton);
            postButton.Click += delegate
            {

            };

            Button game1 = FindViewById<Button>(Resource.Id.GameButton1);
            game1.Click += delegate
            {
                try
                {
                    if (GameManager.GetGame(0).LastThrow == null)
                    {
                        var throwActivity = new Intent(this, typeof(ThrowActivity));
                        throwActivity.PutExtra("gameid", GameManager.GetGame(0).ID.ToString());
                        StartActivity(throwActivity);
                    }
                    else
                    {
                        var recieveActivity = new Intent(this, typeof(RecieveActivity));
                        recieveActivity.PutExtra("gameid", GameManager.GetGame(0).ID.ToString());
                        StartActivity(recieveActivity);
                    }
                }
                catch (IndexOutOfRangeException e)
                {
                    game1.SetBackgroundColor(Android.Graphics.Color.Red);
                }
            };

            Button game2 = FindViewById<Button>(Resource.Id.GameButton2);
            game2.Click += delegate
            {
                try
                {
                    if (GameManager.GetGame(1).LastThrow == null)
                    {
                        var throwActivity = new Intent(this, typeof(ThrowActivity));
                        throwActivity.PutExtra("gameid", GameManager.GetGame(1).ID.ToString());
                        StartActivity(throwActivity);
                    }
                    else
                    {
                        var recieveActivity = new Intent(this, typeof(RecieveActivity));
                        recieveActivity.PutExtra("gameid", GameManager.GetGame(1).ID.ToString());
                        StartActivity(recieveActivity);
                    }
                }
                catch (IndexOutOfRangeException e)
                {
                    game2.SetBackgroundColor(Android.Graphics.Color.Red);
                }
            };

            //Button playButton = FindViewById<Button>(Resource.Id.PlayButton);
            //playButton.Click += delegate
            //{
            //    var throwActivity = new Intent(this, typeof(ThrowActivity));
            //    try
            //    {
            //        throwActivity.PutExtra("gameid", GameManager.GetGame(0).ID.ToString());
            //    }
            //    catch (IndexOutOfRangeException e)
            //    {
            //        playButton.SetBackgroundColor(Android.Graphics.Color.Red);
            //    }
            //    StartActivity(throwActivity);
            //};
        }
    }
}

