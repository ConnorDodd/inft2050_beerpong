using Android.App;
using Android.Content.PM;
using Android.OS;
using BeerPongGame;
using CocosSharp;
using Microsoft.Xna.Framework;

namespace BeerPong
{
    [Activity(
        Label = "BeerPong",
        AlwaysRetainTaskState = false,
        Icon = "@drawable/Icon",
        Theme = "@android:style/Theme.NoTitleBar",
        ScreenOrientation = ScreenOrientation.Portrait,
        LaunchMode = LaunchMode.SingleInstance,
        MainLauncher = false,
        ConfigurationChanges = ConfigChanges.Keyboard | ConfigChanges.KeyboardHidden)
    ]
    class RecieveActivity : AndroidGameActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            var application = new CCApplication();
            var gameDelegate = new GameApplicationDelegate(GameApplicationDelegate.RECIEVE_GAME);
            gameDelegate.gameid = Intent.GetStringExtra("gameid");
            gameDelegate.activity = this;
            application.ApplicationDelegate = gameDelegate;
            SetContentView(application.AndroidContentView);
            application.StartGame();
        }
    }
}