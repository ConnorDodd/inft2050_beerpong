﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace BeerPongGame.CloudConnection
{
    class CloudManager
    {
        public static string GetPostFromServer()
        {
            string toReturn;
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://localhost:50259/api/BeerPong");
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Method = "POST";
            httpWebRequest.Accept = "application/json; charset=utf-8";

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                toReturn = result.ToString();
            }

            return toReturn;
        }
    }
}
