﻿using BeerPongGame.CustomException;
using BeerPongGame.Model;
using CocosSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerPongGame
{
    class RecieveBallLayer : CCLayerColor
    {
        CCDrawNode line, labelBackground;
        CCLabel startLabel;
        Ball ball;
        string gameid;
        Game game;
        int sitTimer;

        public RecieveBallLayer(Android.App.Activity activity, string gameid)
        {
            Color = CCColor3B.DarkGray;
            Opacity = 255;

            var touchListener = new CCEventListenerTouchAllAtOnce();
            touchListener.OnTouchesBegan = OnTouchBegan;
            AddEventListener(touchListener, this);

            //Find the game that was passed from MainActivity
            this.gameid = gameid;
            game = GameManager.GetGame(gameid);

            //Check the state is valid
            ball = new Ball();
            try
            {
                ball.AddForce(game.LastThrow.X, -game.LastThrow.Y);
            }
            catch
            {
                throw new WrongGameStateException("There are no throws in this game yet");
            }

            for (int i = 0; i < game.Cups.Length; i++)
            {
                if (game.Cups[i].IsActive)
                {
                    game.Cups[i].LoadSprite(Cup.CATCH_SPRITE);
                    AddChild(game.Cups[i].Sprite);
                }
            }
            

            line = new CCDrawNode();
            AddChild(line);
            
            AddChild(ball.Sprite);

            StartScheduling();
        }

        protected override void AddedToScene()
        {
            base.AddedToScene();

            SetCups();
            ball.SetPosition2D(Window.WindowSizeInPixels.Width / 2, Window.WindowSizeInPixels.Height - 128);
            ball.Position.Z = 1;
            ball.Sprite.ContentSize = new CCSize(Ball.SIZE, Ball.SIZE);

            line.DrawLine(new CCPoint(0, Window.WindowSizeInPixels.Height - Window.WindowSizeInPixels.Height / 8),
                new CCPoint(Window.WindowSizeInPixels.Width, Window.WindowSizeInPixels.Height - Window.WindowSizeInPixels.Height / 8), 6.0f, CCColor4B.Black);

            labelBackground = new CCDrawNode()
            {
                ContentSize = new CCSize(VisibleBoundsWorldspace.MaxX, VisibleBoundsWorldspace.MaxY)
            };
            labelBackground.DrawRect(VisibleBoundsWorldspace, new CCColor4B(0, 0, 0, 0.8f));
            AddChild(labelBackground);

            startLabel = new CCLabel("TAP TO SHOW", "bottles", 52)
            {
                Position = Window.WindowSizeInPixels.Center,
                Color = CCColor3B.Red,
                HorizontalAlignment = CCTextAlignment.Center,
                VerticalAlignment = CCVerticalTextAlignment.Center,
                AnchorPoint = CCPoint.AnchorMiddle,
                Dimensions = ContentSize
            };
            AddChild(startLabel);
        }

        private void SetCups()
        {
            float originX = Window.WindowSizeInPixels.Width / 2, originY = (Window.WindowSizeInPixels.Height / 6) * 2;
            int cupNumber = 0;
            for (int i = 1; i < 5; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    //cups[cupNumber].SetPosition2D(originX + i * Cup.SIZE, originY - (Cup.SIZE * i) / 2 + Cup.SIZE * j);
                    float cupX = originX - ((i - 1) * Cup.WIDTH) / 2 + (Cup.WIDTH * j) - Cup.WIDTH,
                        cupY = originY - (i * Cup.HEIGHT);
                    game.Cups[cupNumber].SetPosition2D(cupX, cupY);
                    game.Cups[cupNumber].Sprite.ContentSize = new CCSize(Cup.SIZE, Cup.SIZE);
                    cupNumber++;
                }
            }
        }

        private void StartScheduling()
        {
            Schedule(t =>
            {
                ball.Update(game.Cups);

                if (ball.Ended >= 0)
                {
                    if (sitTimer > 100)
                    {
                        FinishScene();
                    }
                    else
                    {
                        sitTimer++;
                    }
                }
                else if (ball.Started && (ball.Acceleration.Y < 1 || ball.OffScreen(Window.WindowSizeInPixels)))
                {
                    if (sitTimer > 150)
                    {
                        FinishScene();
                    }
                    else
                    {
                        sitTimer++;
                    }
                }
            });
        }

        private void FinishScene()
        {
            for (int i = 0; i < game.Cups.Length; i++)
            {
                if (game.Cups[i].Active == Cup.ACTIVE_HALF)
                {
                    game.Cups[i].Active = Cup.ACTIVE_NONE;
                }
            }

            game.LastThrow = null;

            Android.App.Activity gameActivity = Application.AndroidContentView.Context as Android.App.Activity;
            gameActivity.Finish();
        }

        private void OnTouchBegan(List<CCTouch> touch, CCEvent e)
        {
            RemoveChild(labelBackground);
            RemoveChild(startLabel);

            ball.Start();
        }

        public static CCScene CreateGameScene(CCWindow window, Android.App.Activity activity, string gameid)
        {
            var scene = new CCScene(window);
            var layer = new RecieveBallLayer(activity, gameid);

            scene.AddChild(layer);

            return scene;
        }
    }
}
