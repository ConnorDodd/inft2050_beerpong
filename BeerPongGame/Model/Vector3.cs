﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerPongGame.Model
{
    class Vector3
    {
        private float x, y, z;

        public float X { get { return x; } set { x = value; } }
        public float Y { get { return y; } set { y = value; } }
        public float Z { get { return z; } set { z = value; } }

        public Vector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Vector3() { x = 0; y = 0; z = 0; }

        public static Vector3 operator +(Vector3 v1, Vector3 v2)
        {
            return new Vector3(v1.X + v2.x, v1.y + v2.y, v1.z + v2.z);
        }

        public void Offset2D(float x, float y)
        {
            this.x += x;
            this.y += y;
        }

        public void Offset3D (float x, float y, float z)
        {
            this.x += x;
            this.y += y;
            this.z += z;
        }
    }
}
