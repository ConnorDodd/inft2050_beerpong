﻿using CocosSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerPongGame.Model
{
    class Ball
    {
        private const float GRAVITY = -8.0f, DAMP = 0.988f;
        public const int SIZE = 128;
        private float minZ = 0.5f, maxZ = 1.0f;

        private CCSprite sprite;
        public CCSprite Sprite { get { return sprite; } }

        private Vector3 position, velocity, acceleration;

        public Vector3 Position { get { return position; } }
        public Vector3 Velocity { get { return velocity; } }
        public Vector3 Acceleration { get { return acceleration; } }

        private bool started;
        public bool Started { get { return started; } }

        private int ended = -1;
        public int Ended { get { return ended; } }

        public Ball()
        {
            sprite = new CCSprite("ball");

            position = new Vector3();
            velocity = new Vector3();
            acceleration = new Vector3();
        }

        public void Update(Cup[] cups)
        {
            if (!started)
                return;

            acceleration.Y *= DAMP;
            acceleration.X *= DAMP;
            //position += acceleration;
            Vector3 oldPosition = new Vector3(position.X, position.Y, position.Z);

            position.X += acceleration.X;
            position.Y += acceleration.Y;

            if (position.Z < 0.64f)
            {
                for(int i = 0; i < cups.Length; i++)
                {
                    if (!cups[i].IsActive)
                    {
                        continue;
                    }
                    else if (cups[i].CheckDistance(this) < Cup.SIZE / 2)
                    {
                        started = false;
                        ended = i;
                        cups[i].Active = Cup.ACTIVE_HALF;
                        cups[i].Sprite.Parent.RemoveChild(cups[i].Sprite);
                        return;
                    }
                    else if (cups[i].CheckDistance(this) < (Sprite.ScaledContentSize.Width / 2 + Cup.SIZE / 2))
                    {
                        position.X = oldPosition.X;
                        position.Y = oldPosition.Y;

                        acceleration.X = -acceleration.X;
                        acceleration.Y = -acceleration.Y;

                        break;
                    }
                }
            }

            position.Z += acceleration.Z;
            if (position.Z < minZ)
            {
                position.Z = minZ;
                acceleration.Z = -acceleration.Z;
                maxZ *= 0.85f;
            }
            else if (position.Z > maxZ)
            {
                position.Z = maxZ;
                acceleration.Z = -acceleration.Z;
            }

            sprite.Position = new CCPoint(position.X, position.Y);
            sprite.Scale = position.Z;
        }

        public void SetPosition2D(float x, float y)
        {
            position.X = x;
            position.Y = y;

            sprite.Position = new CCPoint(position.X, position.Y);
        }

        public void AddForce(float x, float y)
        {
            x *= 0.08f;
            y *= 0.17f;
            acceleration.Offset2D(x, y);
        }

        public bool OffScreen(CCSize screen)
        {
            return (position.Y > screen.Height || position.Y < 0 || position.X < 0 || position.X > screen.Width);
        }

        public void Start()
        {
            started = true;

            acceleration.Z = -0.01f;
        }
    }
}
