﻿using CocosSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerPongGame.Model
{
    class Cup
    {
        public const int SIZE = 208, ACTIVE_NONE = -1, ACTIVE = 1, ACTIVE_HALF = 0;
        public const float WIDTH = SIZE * 0.8f, HEIGHT = SIZE / 2.0f;
        public const string SPRITE_PATH = "cup", THROW_SPRITE = "cup", CATCH_SPRITE = "bluecup";

        private CCSprite sprite;
        public CCSprite Sprite { get { return sprite; } }

        private Vector3 position;
        public Vector3 Position { get { return position; } }

        private int active = ACTIVE;
        public int Active { get { return active; } set { active = value; } }
        public bool IsActive {  get { return active >= 0; } }

        public Cup()
        {
            sprite = null;
            position = new Vector3();
        }

        public void LoadSprite(string path)
        {
            sprite = new CCSprite(path);
        }

        public void SetPosition2D(float x, float y)
        {
            position.X = x;
            position.Y = y;

            sprite.Position = new CCPoint(position.X, position.Y);
        }

        public double CheckDistance(Ball ball)
        {
            float x = position.X - ball.Position.X,
                y = position.Y - ball.Position.Y;
            double distance = Math.Sqrt(x * x + y * y);

            return distance;
        }
    }
}
