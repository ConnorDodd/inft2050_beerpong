﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerPongGame.Model
{
    class Game
    {
        private Cup[] cups;
        public Cup[] Cups { get { return cups; } }

        private Guid id;
        public Guid ID { get { return id; } }

        public Vector3 LastThrow = null;

        public Game()
        {
            cups = new Cup[10];
            for (int i = 0; i < cups.Length; i++)
            {
                cups[i] = new Cup();
            }
            id = Guid.NewGuid();
        }


    }
}
