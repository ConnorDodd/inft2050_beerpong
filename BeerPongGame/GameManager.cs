﻿using BeerPongGame.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerPongGame
{
    class GameManager
    {
        private static List<Game> games;
        public static List<Game> Games { get { return games; } }

        public static Game GetGame(int index)
        {
            try
            {
                return games[index];
            }
            catch
            {
                throw new IndexOutOfRangeException("The index: " + index + " is out of bounds.");
            }
        }

        public static Game GetGame(string gameid)
        {
            IEnumerator<Game> i = games.GetEnumerator();

            while (i.MoveNext())
            {
                if (i.Current.ID.ToString().Equals(gameid))
                {
                    return i.Current;
                }
            }

            throw new KeyNotFoundException("No game with game ID: " + gameid + " could be found.");
        }

        public static void LoadGames()
        {
            games = new List<Game>();

            games.Add(new Game());
            games.Add(new Game());
        }
    }
}
