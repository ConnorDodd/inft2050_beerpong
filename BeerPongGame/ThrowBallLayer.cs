﻿using System;
using System.Collections.Generic;
using System.Text;
using CocosSharp;
using BeerPongGame.Model;
using Android.Content;

namespace BeerPongGame
{
    public class ThrowBallLayer : CCLayerColor
    {
        public string gameid;
        Game game;
        Ball ball;
        int sitTimer = 0;
        CCDrawNode line, dot;
        CCPoint touchBegan = CCPoint.NegativeInfinity;

        public ThrowBallLayer(Android.App.Activity activity, string gameid) : base()
        {
            var touchListener = new CCEventListenerTouchOneByOne();
            touchListener.OnTouchBegan = OnTouchBegan;
            touchListener.OnTouchEnded = OnTouchEnded;
            /*touchListener.OnTouchesEnded = delegate
            {
                Activity gameActivity = Application.AndroidContentView.Context as Activity;
                gameActivity.Finish();
            };*/

            AddEventListener(touchListener, this);

            Color = CCColor3B.DarkGray;
            Opacity = 255;

            this.gameid = gameid;
            game = GameManager.GetGame(gameid);
            for (int i = game.Cups.Length-1; i >= 0; i--)
            {
                if (game.Cups[i].IsActive)
                {
                    game.Cups[i].LoadSprite(Cup.THROW_SPRITE);
                    AddChild(game.Cups[i].Sprite);
                }
            }

            line = new CCDrawNode();
            AddChild(line);
            ball = new Ball();
            AddChild(ball.Sprite);
            dot = new CCDrawNode();
            AddChild(dot);

            StartScheduling();
        }

        protected override void AddedToScene()
        {
            base.AddedToScene();

            SetCups();
            ball.SetPosition2D(Window.WindowSizeInPixels.Width / 2, 128);
            ball.Position.Z = 1;
            ball.Sprite.ContentSize = new CCSize(Ball.SIZE, Ball.SIZE);

            line.DrawLine(new CCPoint(0, Window.WindowSizeInPixels.Height/8), 
                new CCPoint(Window.WindowSizeInPixels.Width, Window.WindowSizeInPixels.Height/8), 6.0f, CCColor4B.Black);
        }

        private void SetCups()
        {
            float originX = Window.WindowSizeInPixels.Width / 2, originY = (Window.WindowSizeInPixels.Height / 6) * 4;
            int cupNumber = 0;
            for (int i = 1; i < 5; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    //cups[cupNumber].SetPosition2D(originX + i * Cup.SIZE, originY - (Cup.SIZE * i) / 2 + Cup.SIZE * j);
                    float cupX = originX - ((i - 1) * Cup.WIDTH) / 2 + (Cup.WIDTH * j) - Cup.WIDTH,
                        cupY = originY + (i * Cup.HEIGHT);
                    game.Cups[cupNumber].SetPosition2D(cupX, cupY);
                    game.Cups[cupNumber].Sprite.ContentSize = new CCSize(Cup.SIZE, Cup.SIZE);
                    cupNumber++;
                }
            }
            dot.DrawDot(new CCPoint(originX, originY), 4, CCColor4B.Red);
        }

        private void StartScheduling()
        {
            Schedule(t =>
            {
                ball.Update(game.Cups);

                if (ball.Ended >= 0)
                {
                    if (sitTimer > 100)
                    {
                        Android.App.Activity gameActivity = Application.AndroidContentView.Context as Android.App.Activity;
                        gameActivity.Finish();
                    }
                    else
                    {
                        sitTimer++;
                    }
                }
                else if (ball.Started && ball.Acceleration.Y < 1)
                {
                    if (sitTimer > 150)
                    {
                        Android.App.Activity gameActivity = Application.AndroidContentView.Context as Android.App.Activity;
                        gameActivity.Finish();
                    }
                    else
                    {
                        sitTimer++;
                    }
                }
            });
        }

        private bool OnTouchBegan(CCTouch touch, CCEvent e) {
            if (ball.Started)
                return false;
            float x = touch.Location.X - ball.Position.X,
                y = touch.Location.Y - ball.Position.Y;
            double distance = Math.Sqrt(x * x + y * y);

            if (distance < ball.Sprite.ContentSize.Width / 2)
            {
                touchBegan = touch.Location;
                return true;
            }
            else
            {
                touchBegan = CCPoint.NegativeInfinity;
                return false;
            }
        }

        private void OnTouchEnded(CCTouch touch, CCEvent e)
        {
            if (touchBegan != CCPoint.NegativeInfinity)
            {
                //float x = touch.Location.X - touchBegan.X,
                //y = touch.Location.Y - touchBegan.Y;
                //double distance = Math.Sqrt(x * x + y * y);

                float x = touch.Location.X - touchBegan.X,
                    y = touch.Location.Y - touchBegan.Y;

                game.LastThrow = new Vector3(x, y, 0);
                ball.AddForce(x, y);
                ball.Start();
            }
        }

        public static CCScene CreateGameScene(CCWindow window, Android.App.Activity activity, string gameid)
        {
            var scene = new CCScene(window);
            var layer = new ThrowBallLayer(activity, gameid);

            scene.AddChild(layer);

            return scene;
        }
    }
}
