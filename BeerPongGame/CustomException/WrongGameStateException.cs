﻿using System;

namespace BeerPongGame.CustomException
{
    class WrongGameStateException : Exception
    {
        public WrongGameStateException()
        {
        }

        public WrongGameStateException(string message) : base(message)
        {
        }

        public WrongGameStateException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}
