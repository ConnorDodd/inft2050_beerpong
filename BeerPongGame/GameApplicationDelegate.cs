﻿using BeerPongGame.CustomException;
using CocosSharp;
using System;

namespace BeerPongGame
{
    public class GameApplicationDelegate : CCApplicationDelegate
    {
        public Android.App.Activity activity;

        public string gameid;

        public const int THROW_GAME = 1, RECIEVE_GAME = 2;
        private int gameType;
         
        public GameApplicationDelegate(int gameType)
        {
            this.gameType = gameType;
        }

        public override void ApplicationDidFinishLaunching(CCApplication application, CCWindow window)
        {
            application.PreferMultiSampling = false;
            application.ContentRootDirectory = "Content";
            application.ContentSearchPaths.Add("hd");

            Console.WriteLine(application.ContentRootDirectory.ToString());

            CCSize windowSize = window.WindowSizeInPixels;
            CCScene.SetDefaultDesignResolution(windowSize.Width, windowSize.Height, CCSceneResolutionPolicy.ShowAll);

            CCScene scene = null;
            try
            {
                switch (gameType)
                {
                    case THROW_GAME:
                        scene = ThrowBallLayer.CreateGameScene(window, activity, gameid);
                        break;
                    case RECIEVE_GAME:
                        scene = RecieveBallLayer.CreateGameScene(window, activity, gameid);
                        break;
                    default:
                        break;
                }
            }
            catch (WrongGameStateException e)
            {
                Android.App.Activity gameActivity = application.AndroidContentView.Context as Android.App.Activity;
                gameActivity.Finish();
                return;
            }
            window.RunWithScene(scene);
        }

        public override void ApplicationDidEnterBackground(CCApplication application)
        {
            //base.ApplicationDidEnterBackground(application);

            application.Paused = true;
        }

        public override void ApplicationWillEnterForeground(CCApplication application)
        {
            //base.ApplicationWillEnterForeground(application);

            application.Paused = false;
        }
    }
}
